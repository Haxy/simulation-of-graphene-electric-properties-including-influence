\chapter*{Notation and basic definitions}
\noindent Like in most of scientific literature (unlike programming languages used in this thesis) indexing will start from 1 (1, 2, \dots , n) unless stated otherwise.

\noindent Symbol $\delta_{(i,j)}$ will denote Kronecker delta, that is
\begin{eqnarray*}
 \delta_{(i,j)} = \left \{
 \begin{array}{lc}
 1 & \Rightarrow \quad i = j \\
 0 & \Rightarrow \quad i \neq j 
 \end{array}
 \right .
\end{eqnarray*}

\noindent Partial derivative of function $f\left ( \vec{x} \right )$ with respect to argument $x_i$ will be denoted as $\frac{\partial f\left ( \vec{x} \right )}{\partial x_i}$. It may be defined as\footnote{Details of vector algebra used in this definition will be presented later in this chapter.}

\begin{eqnarray*} 
 \left ( \vec{x} = \sum_{i = 1}^{n} x_i \baseVec \right ) \Rightarrow \forall_{i \in \mathbb{N} \cap \langle 1,\,n \rangle},\, \frac{\partial f\left ( \vec{x} \right )}{\partial x_i} = \lim_{h \to 0} \frac{f\left ( \vec{x} + h\baseVec \right ) - f\left ( \vec{x} \right )}{h}
\end{eqnarray*}

\section*{Matrix algebra}
\noindent Base matrix $\baseMat$ will be denoted as
\begin{eqnarray*}
\baseMat = \left (
 \begin{array}{cccc}
  \delta_{(1,i)} \delta_{(1,j)} & \delta_{(1,i)} \delta_{(2,j)} & \cdots & \delta_{(1,i)} \delta_{(n,j)} \\
  \delta_{(2,i)} \delta_{(1,j)} & \delta_{(2,i)} \delta_{(2,j)} & \cdots & \delta_{(2,i)} \delta_{(n,j)} \\
  \vdots & \vdots & \ddots & \vdots \\
  \delta_{(m,i)} \delta_{(1,j)} & \delta_{(m,i)} \delta_{(2,j)} & \cdots & \delta_{(m,i)} \delta_{(n,j)}
 \end{array}
 \right )
\end{eqnarray*}

\noindent Matrix named A will be denoted as
\begin{eqnarray*}
\mat{A} = \left (
 \begin{array}{cccc}
  a_{(1,1)} & a_{(1,2)} & \cdots & a_{(1,n)} \\
  a_{(2,1)} & a_{(2,2)} & \cdots & a_{(2,n)} \\
  \vdots & \vdots & \ddots & \vdots \\
  a_{(m,1)} & a_{(m,2)} & \cdots & a_{(m,n)}
 \end{array}
 \right ) = \sum_{i = 1}^{m} \sum_{j = 1}^{n} a_{(i,j)} \baseMat
\end{eqnarray*}
If $n = m$ then $\mat{A}$ will be called square matrix.

%\pagebreak

\noindent Multiplication of scalar $s$ and matrix $\mat{A}$ will be denoted $s\mat{A}$ and can be simply expressed as
\begin{eqnarray*}
\mat{A} = \sum_{i = 1}^{m} \sum_{j = 1}^{n} a_{(i,j)} \baseMat \Rightarrow s \mat{A} = \left (
 \begin{array}{cccc}
  s a_{(1,1)} & s a_{(1,2)} & \cdots & s a_{(1,n)} \\
  s a_{(2,1)} & s a_{(2,2)} & \cdots & s a_{(2,n)} \\
  \vdots & \vdots & \ddots & \vdots \\
  s a_{(m,1)} & s a_{(m,2)} & \cdots & s a_{(m,n)}
 \end{array}
 \right ) = \mat{A} s
\end{eqnarray*}
What easily can be noticed $s\mat{A} = \mat{A} s$.

\noindent Addition of matrices $\mat{A}$ and $\mat{B}$ is
\begin{eqnarray*}
\left (\mat{A} = \sum_{i = 1}^{m} \sum_{j = 1}^{n} a_{(i,j)} \baseMat \land \mat{B} = \sum_{i = 1}^{m} \sum_{j = 1}^{n} b_{(i,j)} \baseMat \right ) \Rightarrow\\
 \Rightarrow \mat{A} + \mat{B} = \left (
 \begin{array}{cccc}
  a_{(1,1)} + b_{(1,1)} & a_{(1,2)} + b_{(1,2)} & \cdots & a_{(1,n)} + b_{(1,n)} \\
  a_{(2,1)} + b_{(2,1)} & a_{(2,2)} + b_{(2,2)} & \cdots & a_{(2,n)} + b_{(2,n)} \\
  \vdots & \vdots & \ddots & \vdots \\
  a_{(m,1)} + b_{(m,1)} & a_{(m,2)} + b_{(m,2)} & \cdots & a_{(m,n)} + b_{(m,n)}
 \end{array}
 \right ) = \mat{B} + \mat{A}
\end{eqnarray*}
What easily can be noticed $\mat{A} + \mat{B} = \mat{B} + \mat{A}$.

\noindent Multiplication of matrices $\mat{A}$ and $\mat{B}$ denoted as $\mat{A} \mat{B}$ is
\begin{eqnarray*}
\left (\mat{A} = \sum_{i = 1}^{m_1} \sum_{j = 1}^{n} a_{(i,j)} \baseMat \land \mat{B} = \sum_{i = 1}^{n} \sum_{j = 1}^{m_2} b_{(i,j)} \baseMat \right ) \Rightarrow\\
 \Rightarrow \mat{A} \mat{B} = \sum_{i = 1}^{m_1} \sum_{j = 1}^{m_2} \sum_{k = 1}^{n} a_{(i,k)} b_{(k,j)} \baseMat
\end{eqnarray*}
What easily can be noticed, in general case $\mat{A} \mat{B} \neq \mat{B} \mat{A}$.

\noindent Transpose of matrix $\mat{A}$ will be denoted as
\begin{eqnarray*}
\mat{A} = \sum_{i = 1}^{m} \sum_{j = 1}^{n} a_{(i,j)} \baseMat \Rightarrow
 \left (\mat{A} \right )^{T} = \sum_{i = 1}^{m} \sum_{j = 1}^{n} a_{(j,i)} \baseMat
\end{eqnarray*}

\noindent Determinant of matrix $\mat{A}$ will be denoted as
\begin{eqnarray*}
 \mat{A} = \sum_{i = 1}^{n} \sum_{j = 1}^{n} a_{(i,j)} \baseMat \Rightarrow \det{\mat{A}} = \Bigg \{
 \begin{array}{lc}
 a_{(1,1)} & \Rightarrow \quad n = 1 \\
 \forall_{j \in \mathbb{N} \cap \langle 1, n \rangle}, \quad \sum_{i = 1}^{n} (-1)^{i + j} a_{(i,j)} M_{(i,j)} & \Rightarrow \quad n \neq 1 
 \end{array}
\end{eqnarray*}
where minor $M_{(i,j)}$ is determinant of matrix that results from $\mat{A}$ by removing row $i$ and column $j$. Pleas note that this is recursive definition. Calculating minor requires calculation of smaller matrix determinant and this recursion is ended when one reach matrix composed of single element.

\noindent Adjugate matrix of matrix $\mat{A}$ will be denoted as
\begin{eqnarray*}
\mat{A} = \sum_{i = 1}^{n} \sum_{j = 1}^{n} a_{(i,j)} \baseMat \Rightarrow \adj{\mat{A}} = \left (\sum_{i = 1}^{n} \sum_{j = 1}^{n} (-1)^{i + j} M_{(i,j)} \baseMat \right )^{T}
\end{eqnarray*}
where minor $M_{(i,j)}$ is defined like in case of determinant of matrix.

\noindent Identity matrix $\mat{I}$ will be denoted as
\begin{eqnarray*}
\mat{I} = \sum_{i = 1}^{n} \sum_{j = 1}^{n} \delta_{(i,j)} \baseMat
\end{eqnarray*}

\noindent Inversion of matrix $\mat{A}$ will be denoted as
\begin{eqnarray*}
 \mat{A} = \sum_{i = 1}^{n} \sum_{j = 1}^{n} a_{(i,j)} \baseMat \Rightarrow \left (\mat{A} \right )^{-1} = \frac{1}{\det{\mat{A}}} \adj{\mat{A}}
\end{eqnarray*}
It is worth noticing that $\mat{A} \left (\mat{A} \right )^{-1} = \left (\mat{A} \right )^{-1} \mat{A} = \mat{I}$.

\noindent Inner product of matrices\footnote{This is just extension of concept presented in \cite{mathworld-inner} to matrices tharefore notation and naming conventions is preserved.} $\mat{A}$ and $\mat{B}$ will be denoted as $\innerMul{\mat{A}}{\mat{B}}$ and may be defined as
\begin{eqnarray*}
 \left ( \mat{A} = \sum_{i = 1}^{n} \sum_{j = 1}^{m} a_{(i,j)} \baseMat \land \mat{B} = \sum_{i = 1}^{n} \sum_{j = 1}^{m} b_{(i,j)} \baseMat \right ) \Rightarrow \\
 \Rightarrow \innerMul{\mat{A}}{\mat{B}} = \sum_{j = 1}^{m} \left ( \dotMul{ \left ( \sum_{j = 1}^{n} a_{(i,j)} \baseVec \right ) }{\left ( \sum_{j = 1}^{n} b_{(i,j)} \baseVec \right ) } \right ) \baseVec[j]
\end{eqnarray*}
In the above deffinition comumns of both matrices are treated as vectors\footnote{Vectors will be defined later in this chapter} dot multipled reducing matrices to vector. Actually the same concept could be row--wise however column--wise will be used in this paper since it has simpler implementation in GLSL.

\noindent Element--wise multiplication of matrices $\mat{A}$ and $\mat{B}$ will be denoted\footnote{This operation is Hadamard product, that is usualy denoted as $\mat{A} \circ \mat{B}$. However this notation is used to make it uniform with elemet--wise multiplication of vectors. The explanation for such notation in case of vectors will be presented later in this chapter.} as $\compMul{\mat{A}}{\mat{B}}$ and may be defines as
\begin{eqnarray*}
 \left ( \mat{A} = \sum_{i = 1}^{n} \sum_{j = 1}^{m} a_{(i,j)} \baseMat \land \mat{B} = \sum_{i = 1}^{n} \sum_{j = 1}^{m} b_{(i,j)} \baseMat \right ) \Rightarrow\\
 \Rightarrow \compMul{\mat{A}}{\mat{B}} = \sum_{i = 1}^{n} \sum_{j = 1}^{m} a_{(i,j)} b_{(i,j)} \baseMat
\end{eqnarray*}

\noindent Matrix of all ones (do not mistake with identity matrix $\mat{I}$) will be denoted as $\mat{1}$ and may be defined as
\begin{eqnarray*}
 \mat{1} = \sum_{i = 1}^{n} \sum_{j = 1}^{m} \baseMat
\end{eqnarray*}

\section*{Vector algebra}
\noindent Base vector $\baseVec$ will be denoted as
\begin{eqnarray*}
 \baseVec = \left (
 \begin{array}{cccc}
  \delta_{(1,i)} & \delta_{(2,i)} & \cdots & \delta_{(n,i)}
 \end{array}
\right )
\end{eqnarray*}

\noindent Vector named v will be denoted as
\begin{eqnarray*}
 \vec{v} = \left (
 \begin{array}{cccc}
  v_1 & v_2 & \cdots & v_n
 \end{array}
\right ) = \sum_{i = 1}^{n} v_i \baseVec
\end{eqnarray*}

\noindent Multiplication of scalar $s$ and vector $\vec{v}$ will be denoted $s\vec{v}$ and can be simply expressed as
\begin{eqnarray*}
\vec{v} = \sum_{i = 1}^{n} v_i \baseVec \Rightarrow s \vec{v} = \left (
 \begin{array}{cccc}
  s v_1 & s v_2 & \cdots & s v_n
 \end{array}
 \right ) = \vec{v} s
\end{eqnarray*}
What easily can be noticed $s\vec{v} = \vec{v} s$.

\noindent Addition of vectors $\vec{a}$ and $\vec{b}$ is
\begin{eqnarray*}
\left (\vec{a} = \sum_{i = 1}^{n} a_i \baseVec \land \vec{b} = \sum_{i = 1}^{n} b_i \baseVec \right ) \Rightarrow\\
 \Rightarrow \vec{a} + \vec{b} = \left (
 \begin{array}{cccc}
  a_1 + b_1 & a_2 + b_2 & \cdots & a_n + b_n
 \end{array}
 \right ) = \vec{b} + \vec{a}
\end{eqnarray*}
What easily can be noticed $\vec{a} + \vec{b} = \vec{b} + \vec{a}$.

\noindent Dot product of vectors $\vec{a}$ and $\vec{b}$ will be denoted as simple vector multiplication
\begin{eqnarray*}
\left (\vec{a} = \sum_{i = 1}^{n} a_i \baseVec \land \vec{b} = \sum_{i = 1}^{n} b_i \baseVec \right ) \Rightarrow \dotMul{\vec{a}}{\vec{b}} = \sum_{i = 1}^{n} a_i b_i
\end{eqnarray*}

\noindent Cross product of vectors having $n$ elements will be considered as (in general case) operator of $n - 1$ operands and will be denoted as $\cross{{\vec{x^1}}{\vec{x^2}}{\dots}{\vec{v^{n - 1}}}}$ where
\begin{eqnarray*}
 \left (\forall_{j \in \mathbb{N} \cap \langle 1,\,(n - 1)\rangle}, \quad \vec{x^j} = \sum_{i = 1}^{n} x_{i}^{j} \baseVec \right ) &\Rightarrow& \cross{{\vec{x^1}}{\vec{x^2}}{\dots}{\vec{v^{n - 1}}}} = \det\left (\sum_{i = 1}^{n} \baseVec \baseMat[1][i] + \sum_{i = 1}^{n - 1} \sum_{j = 1}^{n} x_{j}^{i} \baseMat[i + 1] \right )
\end{eqnarray*}
in simple case for two vectors $\vec{a}$ and $\vec{b}$ of three elements each $\cross{{\vec{a}}{\vec{b}}}$ may be described as
\begin{eqnarray*}
\left (\vec{a} = \sum_{i = 1}^{3} a_i \baseVec \land \vec{b} = \sum_{i = 1}^{3} b_i \baseVec \right ) \Rightarrow \cross{{\vec{a}}{\vec{b}}} = \left (a_2 b_3 - a_3 b_2 \right ) \baseVec[1] + \left (a_1 b_3 - a_3 b_1 \right ) \baseVec[2] + \left (a_1 b_2 - a_2 b_1 \right ) \baseVec[3]
\end{eqnarray*}

\noindent Element--wise multiplication of two vectors $\vec{a}$ and $\vec{b}$ will be denoted as\footnote{This operation is Hadamard product, that is usualy denoted as $\vec{a} \circ \vec{b}$. This notation is not being used due to fact that in this thesis element--wise division is also being used. Unfortunately element--wise division seems not to have any standard symbol for its notation.} $\compMul{\vec{a}}{\vec{b}}$ where
\begin{eqnarray*}
\left (\vec{a} = \sum_{i = 1}^{n} a_i \baseVec \land \vec{b} = \sum_{i = 1}^{n} b_i \baseVec \right ) \Rightarrow \compMul{\vec{a}}{\vec{b}} = \sum_{i = 1}^{n} a_i b_i \baseVec
\end{eqnarray*}

\noindent Element--wise division of two vectors $\vec{a}$ and $\vec{b}$ will be denoted as $\compDiv{\vec{a}}{\vec{b}}$ where
\begin{eqnarray*}
\left (\vec{a} = \sum_{i = 1}^{n} a_i \baseVec \land \vec{b} = \sum_{i = 1}^{n} b_i \baseVec \right ) \Rightarrow \compDiv{\vec{a}}{\vec{b}} = \sum_{i = 1}^{n} \frac{a_i}{b_i} \baseVec
\end{eqnarray*}

\noindent Multiplication of matrix $\mat{A}$ by vector $\vec{v}$ will be denoted $\mat{A} \vec{v}$ and is expressed as
\begin{eqnarray*}
\left (\mat{A} = \sum_{i = 1}^{m} \sum_{j = 1}^{n} a_{(i,j)} \baseMat \land \vec{v} = \sum_{i = 1}^{n} v_i \baseVec \right ) \Rightarrow \mat{A} \vec{v} = \sum_{j = 1}^{m} \left (\sum_{i = 1}^{n} a_{(j,i)} v_i \right ) \baseVec[j]
\end{eqnarray*}

\noindent Multiplication of vector $\vec{v}$ by matrix $\mat{A}$ will be denoted $\vec{v} \mat{A}$ and is expressed as
\begin{eqnarray*}
\left (\mat{A} = \sum_{i = 1}^{m} \sum_{j = 1}^{n} a_{(i,j)} \baseMat \land \vec{v} = \sum_{i = 1}^{m} v_i \baseVec \right ) \Rightarrow \vec{v} \mat{A} = \sum_{j = 1}^{n} \left (\sum_{i = 1}^{m} a_{(i,j)} v_i \right ) \baseVec[j]
\end{eqnarray*}
In general case $\vec{v} \mat{A} \neq \mat{A} \vec{v}$. Although $\vec{v} (\mat{A} \vec{v}) = \vec{v} (\vec{v} \mat{A})$. Moreover $\vec{v} \mat{A} = \left (\mat{A} \right ) ^{T} \vec{v}$ and $\mat{A} \vec{v} = \vec{v} \left ( \mat{A} \right ) ^{T}$.

\noindent Length of vector $\vec{v}$ will be denoted as $\abs{\vec{v}}$ and is expressed as
\begin{eqnarray*}
\abs{\vec{v}} &=& \sqrt{\dotMul{\vec{v}}{\vec{v}}}
\end{eqnarray*}

\noindent Normalised vector $\vec{v}$ (vector parallel to $\vec{v}$ having length $1$) will be denoted as $\hat{\vec{v}}$ and is expressed as
\begin{eqnarray*}
\hat{\vec{v}} &=& \frac{1}{\abs{\vec{v}}} \vec{v}
\end{eqnarray*}

\noindent Vector of ones will be denoted as $\vec{1}$ and is defined as
\begin{eqnarray*}
\vec{1} &=& \sum_{i = 1}^{n} \baseVec
\end{eqnarray*}

\noindent Symbol $\vec{\nabla}$ will denote nabla operator that is defined as
\begin{eqnarray*}
\vec{\nabla} &=& \sum_{i = 1}^{n} \frac{\partial}{\partial x_i} \baseVec
\end{eqnarray*}
Please note that nabla does not correspond to any physical element and only simpifies equations.

\section*{Set algebra and statistics}
\noindent Set of values $a, b, \dots$ will be denoted as $\set{{a}{b}{\dots}}$.

\noindent Set of natural numbers will be denoted as $\mathbb{N}$. In this thesis zero will not be considered as natural number.

\noindent Weighted mean of vector $\vec{x}$ elements set with weights from vector $\vec{w}$ elements set will be denoted as $\arithMean[\vec{w}]{\vec{x}}$ and may be expressed as
\begin{eqnarray*}
 \vec{w} = \sum_{i = 1}^{n} w_i \baseVec &\Rightarrow& \arithMean[\vec{w}]{\vec{x}} = \frac{\dotMul{\vec{x}}{\vec{w}}}{\sum_{i = 1}^n w_i} = \frac{\dotMul{\vec{x}}{\vec{w}}}{\dotMul{\vec{1}}{\vec{w}}}
\end{eqnarray*}

\noindent Arithmetic mean of vector $\vec{x}$ elements set will be denoted as $\arithMean{\vec{x}}$ and may be expressed as
\begin{eqnarray*}
 \arithMean{\vec{x}} &=& \arithMean[\vec{1}]{\vec{x}}
\end{eqnarray*}

\section*{Electrical symbols}
\noindent Electric potential will be represented with letter $\varphi$.

\noindent To simplify equation and programs resistors will be defcribed by means of their conductance ($G$) rather than resistance ($R$), where $G = \frac{1}{R}$.

\noindent Symbol
\begin{circuitikz}
 \ctikzset{label/align = straight}
 \draw (-1,0) to[resistor=$G$] (1,0);
\end{circuitikz}
will represent resistor having conductance $G$.

\noindent Symbol
\begin{circuitikz}
 \ctikzset{label/align = straight}
 \draw (-1,0) to[voltage source=$V$] (1,0);
\end{circuitikz}
will represent source of voltage $V$.
