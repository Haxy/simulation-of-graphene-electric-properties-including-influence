\chapter{Simulation of uniform material without influence of magnetic field}\label{cha:uniform_no_field}
Perfectly isolated material, not affected by magnetic, field may be simply represent as net of resistors without any other elements. In simulation of uniform material one may assume that each resistor has constant conductance. Since in this approach material is perfectly uniform, conductance depends only on distance between nodes. Therefore each resistor has conductance $\frac{G}{l}$ where $l$ is relative distance between nodes and $G$ is constant.

Additionally, since each node is described only by its electric potential and GPUs may easily store vectors of four elements as fragment, it is convenient to consider each fragment as four nodes. Those nodes will be later called R, G\footnote{Do not mistake node G and conductance $G$.}, B and A\footnote{Those abbreviations stands for red, green, blue and alpha channels respectively.}.
\begin{figure}[H]
 \begin{center}
  \begin{circuitikz}
   \ctikzset{label/align = straight}
   \draw (1,2) to[open, o-o] (1,1); %centre G to R
   \draw (2,2) to[open, o-o] (2,1); %centre A to B
   \draw (0,2) to[open, o-o] (0,1); %left A to B
   \draw (3,2) to[open, o-o] (3,1); %right G to R
   \draw (1,3) to[open, o-o] (2,3); %upper R to B
   \draw (1,0) to[open, o-o] (2,0); %lower G to A
   \draw (1,2) node[anchor=west] {G};
   \draw (1,1) node[anchor=west] {R};
   \draw (2,2) node[anchor=east] {A};
   \draw (2,1) node[anchor=east] {B};
   \draw (1.5,1.5) node {centre};
   \draw (0,1) node[anchor=east] {B};
   \draw (0,2) node[anchor=east] {A};
   \draw (-0.5,1.5) node {left};
   \draw (3,2) node[anchor=west] {G};
   \draw (3,1) node[anchor=west] {R};
   \draw (3.5,1.5) node {right};
   \draw (1,3) node[anchor=west] {R};
   \draw (2,3) node[anchor=east] {B};
   \draw (1.5,3.5) node {upper};
   \draw (1,0) node[anchor=west] {G};
   \draw (2,0) node[anchor=east] {A};
   \draw (1.5,-0.5) node {lower};
   \draw[dotted] (0.5,-1) -- (0.5,4);
   \draw[dotted] (2.5,-1) -- (2.5,4);
   \draw[dotted] (-1,0.5) -- (4,0.5);
   \draw[dotted] (-1,2.5) -- (4,2.5);
  \end{circuitikz}
  \caption{placement of R, G, B and A nodes within fragment}
  \label{fig:rgbanodes}
 \end{center}
\end{figure}

\section{Simplest net topology} \label{sec:unifor_no_field_simplest}
Even assuming net composed only of resistors, there are several possible topologies. The simplest one (called later in this document `cross topology') may be illustrated with figure \ref{fig:topology1} (page \pageref{fig:topology1}). In this topology each node is directly influenced at most four of its neighbours (upper, lower, left and right). Each connection is simple resistor with constant conductance. At the borders some connections (and hence resistors) may be missing. In this topology one may safely assume that relative lengths of all connections are the same. This simplifies, giving $l$ equal one and $G$ depending on resolution of simulation.
\begin{figure}%[h]
 \begin{center}
  \input{crossschema.tex}
  \caption{simplest net topology (cross), without influence of magnetic field}
  \label{fig:topology1}
 \end{center}
\end{figure}

\subsection{Internal conditions}
Nodes not being placed on border have connection to all their neighbours. In such case all four resistors are well defined as shown in figure \ref{fig:internalcross}. This may simplify code, since there is no conditional execution required. Unfortunately conditional code may strongly reduce performance. The needs and solutions to escape this problem are described in \cite{engel2012gpu} and \cite{wilt2013cuda}.

\begin{figure}[H]
 \begin{center}
  \begin{circuitikz}
   \ctikzset{label/align = straight}
   \draw (2,0) to[resistor=$G$, o-*] (2,2);
   \draw (2,4) to[resistor=$G$, o-*] (2,2);
   \draw (0,2) to[resistor=$G$, o-*] (2,2);
   \draw (4,2) to[resistor=$G$, o-*] (2,2);
   \draw (2.5,2.5) to[short, o-*] (2,2);
   \draw (2,4) node[anchor=south] {$\varphi_{up\ old}$};
   \draw (0,2) node[anchor=east] {$\varphi_{left\ old}$};
   \draw (2,0) node[anchor=north] {$\varphi_{down\ old}$};
   \draw (4,2) node[anchor=west] {$\varphi_{right\ old}$};
   \draw (2.5,2.5) node[anchor=west] {$\varphi_{centre\ old}$};
  \end{circuitikz}
  \caption{internal node in cross topology}
  \label{fig:internalcross}
 \end{center}
\end{figure}

In order to fulfil Kirchhoff's low for central node from figure \ref{fig:internalcross} one has to modify its electric potential. The difference for this potential to be applied ($\Delta\varphi$) may be simply calculated as
\begin{eqnarray}
 \nonumber G\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{up\ old} \right ) &+& G\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{left\ old} \right ) +\\
 \nonumber + G\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{down\ old} \right ) &+& G\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{right\ old} \right ) = 0\\
 \nonumber 4 G\left (\varphi_{centre\ old} + \Delta\varphi \right  ) &=& G\left (\varphi_{up\ old} + \varphi_{left\ old} + \varphi_{down\ old} + \varphi_{right\ old} \right )
\end{eqnarray}
\begin{equation}
 \Delta\varphi = \frac{\varphi_{up\ old} + \varphi_{left\ old} + \varphi_{down\ old} + \varphi_{right\ old}}{4} - \varphi_{centre\ old}
 \label{eq:crossdelta}
\end{equation}

It should be highlighted that $\Delta\varphi$ does not depend on $G$ and depends only on electric potentials (previous values of neighbours electric potentials and of considered node electric potential).

This may be realised by program
\lstinputlisting[caption={code for equation \ref{eq:potentialchange} for internal conditions of cross topology}, label={code:simpleinternal}, escapechar=, style=customGLSL]{crossinternal.gls}

\pagebreak
In the above code `\variable{dataSampler}' stores values of electric potential that were calculated in previous iteration (`old values'). The `\variable{configSampler}' stores information whether node has forced electric potential ($0$) or not ($1$). This variable corresponds to parameter $c$ from equation \ref{eq:potentialchange} (page \pageref{eq:potentialchange}). The `\variable{calculationDim}' is the resolution of calculation in fragments\footnote{Horizontal resolution is $calculationDim.x$ while vertical $calculationDim.y$.}. Functions `\variable{calculateRSimple}', `\variable{calculateGSimple}', `\variable{calculateBSimple}' and `\variable{calculateASimple}' are computing value of $\Delta\varphi$ for nodes R, G, B and A respectively. They are based on equation \ref{eq:crossdelta} (page \pageref{eq:crossdelta}). The only difference is that instead of fraction there is used dot product. This is due to the fact that most of GPUs use vector instructions and hence dot product may be calculated as single instruction by dedicated hardware rather than three additions and one division or multiplication. This solution will be often used later.

Above code computes each node separately. It is more readable, however not very efficient. One may take more advantages from vector processing and rewrite equation \ref{eq:crossdelta} in vector form

\begin{equation}
 \Delta\vec{\varphi} = \vec{g}\mat{\varphi} - \vec{\varphi}_{centre\ old}
 \label{eq:crossdeltavector}
\end{equation}
where
\begin{eqnarray}
 \nonumber\vec{g} = \sum_{i = 1}^{4}\frac{1}{4} \baseVec
\end{eqnarray}
while
\begin{eqnarray}
 \nonumber\vec{\varphi}_{centre\ old} &=& \varphi^{R}_{centre\ old} \baseVec[1] + \varphi^{G}_{centre\ old} \baseVec[2] + \varphi^{B}_{centre\ old} \baseVec[3] + \varphi^{A}_{centre\ old} \baseVec[4]\\
 \nonumber\vec{\varphi}_{up\ old} &=& \varphi^{R}_{up\ old} \baseVec[1] + \varphi^{G}_{up\ old} \baseVec[2] + \varphi^{B}_{up\ old} \baseVec[3] + \varphi^{A}_{up\ old} \baseVec[4]\\
 \nonumber\vec{\varphi}_{down\ old} &=& \varphi^{R}_{down\ old} \baseVec[1] + \varphi^{G}_{down\ old} \baseVec[2] + \varphi^{B}_{down\ old} \baseVec[3] + \varphi^{A}_{down\ old} \baseVec[4]\\
 \nonumber\vec{\varphi}_{left\ old} &=& \varphi^{R}_{left\ old} \baseVec[1] + \varphi^{G}_{left\ old} \baseVec[2] + \varphi^{B}_{left\ old} \baseVec[3] + \varphi^{A}_{left\ old} \baseVec[4]\\
 \nonumber\vec{\varphi}_{right\ old} &=& \varphi^{R}_{right\ old} \baseVec[1] + \varphi^{G}_{right\ old} \baseVec[2] + \varphi^{B}_{right\ old} \baseVec[3] + \varphi^{A}_{right\ old} \baseVec[4]
\end{eqnarray}
and $\mat{\varphi}$ is matrix with rows being vectors $\vec{\varphi}_{up\ old}$, $\vec{\varphi}_{down\ old}$, $\vec{\varphi}_{left\ old}$ and $\vec{\varphi}_{right\ old}$.

Having $\Delta\vec{\varphi}$ one may calculate $\vec{\varphi}_{centre\ new}$ extending equation \ref{eq:potentialchange} from page \pageref{eq:potentialchange} to vector form

\begin{equation}
\vec{\varphi}_{centre\ new} = \vec{\varphi}_{centre\ old} + (\compMul{\vec{c}}{\Delta\vec{\varphi}})
 \label{eq:ptentialchangevector}
\end{equation}
where
\begin{eqnarray}
 \nonumber\vec{c} = c^{R} \baseVec[1] + c^{G} \baseVec[2] + c^{B} \baseVec[3] + c^{A} \baseVec[4]
\end{eqnarray}

The code below realises above equation. What can be easily noticed it is not as readable as \ref{code:simpleinternal} (on page \pageref{code:simpleinternal}), but it is far shorter and easier in computation. Execution time comparison of codes from listings \ref{code:simpleinternal} and \ref{code:simpleinternalvector} is described in (). %TODO: Add reference for comparison

\lstinputlisting[caption={code for equation \ref{eq:ptentialchangevector} for internal conditions of cross topology}, label={code:simpleinternalvector}, escapechar=, style=customGLSL]{crossinternalvector.gls}

In code above variables `\variable{configSampler}' and `\variable{calculationDim}' have the same role as in code from listing \ref{code:simpleinternal}. Variable `\variable{data}' is same as `\variable{dataSampler}'. Because of that variable `\variable{C}' is corresponding to $\vec{\varphi}_{centre\ old}$ and variable `\variable{Gvec4}' is $\vec{g}$ from equation \ref{eq:crossdeltavector}. Variables `\variable{U}', `\variable{D}', `\variable{R}' and `\variable{L}' are $\vec{\varphi}_{up\ old}$, $\vec{\varphi}_{down\ old}$, $\vec{\varphi}_{right\ old}$ and $\vec{\varphi}_{left\ old}$ respectively. It is also worth reminding that in GLSL constructor for matrices (in the above code it is `\variable{mat4}' type) takes arguments ordered in columns not rows, hence unlike in equation, matrix is multiplied by vector. In the above case each vector (`\variable{vec4}') from this constructor is another column. Moreover operator `\variable{*}' realises in this language element--wise multiplication.

\subsection{Border conditions}Previously it was assumed that nodes being considered in computation have all their neighbours. However in general case it is not always fulfilled. Even assuming uniform material without any lacks, it is neither infinitely large nor closed on a spherical surface. Considered material is having borders, where nodes may miss some of their neighbours. Assuming rectangular shape of simulated material one may find that there are only eight possible configurations of absent neighbours. Those configurations are
\begin{figure}[H]
 \centering
 \input{crossborderplain.tex}
\end{figure}

Having this one may easily rewrite equation \ref{eq:crossdelta} (page \pageref{eq:crossdelta}) to reflect those changes.
\begin{eqnarray}
 \nonumber\Delta\vec{\varphi} &=&\frac{\varphi_{left\ old} + \varphi_{down\ old} + \varphi_{right\ old}}{3} - \varphi_{centre\ old} \quad\Leftrightarrow\quad \mbox{missing only upper node}\\
 \nonumber\Delta\vec{\varphi} &=&\frac{\varphi_{up\ old} + \varphi_{left\ old} + \varphi_{right\ old}}{3} - \varphi_{centre\ old} \quad\Leftrightarrow\quad \mbox{missing only lower node}\\
 \nonumber\Delta\vec{\varphi} &=&\frac{\varphi_{up\ old} + \varphi_{down\ old} + \varphi_{right\ old}}{3} - \varphi_{centre\ old} \quad\Leftrightarrow\quad \mbox{missing only left node}\\
 \nonumber\Delta\vec{\varphi} &=&\frac{\varphi_{up\ old} + \varphi_{left\ old} + \varphi_{down\ old}}{3} - \varphi_{centre\ old} \quad\Leftrightarrow\quad \mbox{missing only right node}\\
 \nonumber\Delta\vec{\varphi} &=&\frac{\varphi_{down\ old} + \varphi_{right\ old}}{2} - \varphi_{centre\ old} \quad\Leftrightarrow\quad \mbox{missing only upper and left nodes}\\
 \nonumber\Delta\vec{\varphi} &=&\frac{\varphi_{left\ old} + \varphi_{down\ old}}{2} - \varphi_{centre\ old} \quad\Leftrightarrow\quad \mbox{missing only upper and right nodes}\\
 \nonumber\Delta\vec{\varphi} &=&\frac{\varphi_{up\ old} + \varphi_{right\ old}}{2} - \varphi_{centre\ old} \quad\Leftrightarrow\quad \mbox{missing only lower and left nodes}\\
 \nonumber\Delta\vec{\varphi} &=&\frac{\varphi_{up\ old} + \varphi_{left\ old}}{2} - \varphi_{centre\ old} \quad\Leftrightarrow\quad \mbox{missing only lower and right nodes}
\end{eqnarray}

However one may assume slightly different topology at the border as presented below
\begin{figure}[H]
 \centering
 \input{crossborderloop.tex}
\end{figure}

In case of topology presented above one may assume that electric potential of missing node is equal to $\varphi_{centre\ old}$ and use both equations \ref{eq:crossdelta} and \ref{eq:crossdeltavector} (pages \pageref{eq:crossdelta} and \pageref{eq:crossdeltavector}). This approach is usefull because it allows for usage of simpler code
\lstinputlisting[caption={part of code for equation \ref{eq:ptentialchangevector} for border conditions of cross topology}, label={code:simplebordervector}, escapechar=, style=customGLSL]{crossbordervectorsimple.gls}

The above code fragment extends one from listing \ref{code:simpleinternalvector} (page \pageref{code:simpleinternalvector}). It is also important to notice that by default (as used in code above) GLSL specifies center of processed fragment. Because of that each element of vector `\variable{gl\_FragCoord}' is in range $\langle0 + \frac{1}{2}, (n - 1) + \frac{1}{2}\rangle$ where $n$ is the size in particular direction. Due to this fact no element of neither `\variable{URpos}' nor `\variable{DLpos}' is zero or one. Thanks to this in code responsible for value calculation of variables `\variable{isUin}', `\variable{isDin}', `\variable{isLin}' and `\variable{isRin}' division by zero is not possible.

Those variables are either zero or one. They have value one if neighbouring node (upper, lower, left or right respectively) is present and zero otherwise. Due to this vectors `\variable{isU}', `\variable{isD}', `\variable{isL}' and `\variable{isR}' are always either $\baseVec[1]$ or $\baseVec[2]$ and scalar product works as selector
\begin{eqnarray}
 \nonumber\vec{a} = \sum_{i = 1}^{n} a_i \baseVec &\Rightarrow& \forall_{j \in \mathbb{N} \cap \langle 1, n \rangle}, \quad\dotMul{\vec{a}}{\baseVec[j]} = a_j
\end{eqnarray}

This approach allows to remove conditional code. Although one may consider absolute value of float from above code as conditional code, such operations are so common and trivial, that they are performed by hardware without any brunching. Such code gives large optimisation and hence strongly increases performance. Unfortunately such optimisation can not be performed automatically by compiler and has to be done manually as described in \cite{gpuGem2ch35} and \cite{wilt2013cuda}.

\section{Star topology}
Another presented topology is star. It may be illustrated with figure \ref{fig:topology2} (page \pageref{fig:topology2}). In this topology each node is directly influenced at most eight of its neighbours (upper, lower, left, right, upper left, upper right, lower left and lower right). Each connection is simple resistor with constant conductance. However unlike in case of cross topology, not all resistors are having the same conductance. This topology is not reflecting real material because distances of neighbouring nodes to computed node are not same, hence they are not having the same time resolution. However this topology is worth considering because of different conductance of connecting resistors.

\begin{figure}%[h]
 \begin{center}
  \input{starschema.tex}
  \caption{star topology, without influence of magnetic field}
  \label{fig:topology2}
 \end{center}
\end{figure}

In this topology one may assume that relative lengths of connections to upper, lower, left and right nodes are equal $1$, while others (upper left, upper right, lower left and lower right) are $\sqrt{2}$. Again one may assume that conductance of each resistor is $\frac{G}{l}$ where $l$ is relative length of connection and $G$ depends on resolution of simulation. This is presented by figure \ref{fig:internalstar} (page \pageref{fig:internalstar}).

\begin{figure}%[H]
 \begin{center}
  \begin{circuitikz}[scale=1.5]
   \ctikzset{label/align = straight}
   \draw (0,0) to[resistor=$\frac{G}{\sqrt{2}}$, o-*] (2,2);
   \draw (2,0) to[resistor=$G$, o-*] (2,2);
   \draw (0,4) to[resistor=$\frac{G}{\sqrt{2}}$, o-*] (2,2);
   \draw (2,4) to[resistor=$G$, o-*] (2,2);
   \draw (0,2) to[resistor=$G$, o-*] (2,2);
   \draw (4,0) to[resistor=$\frac{G}{\sqrt{2}}$, o-*] (2,2);
   \draw (4,2) to[resistor=$G$, o-*] (2,2);
   \draw (4,4) to[resistor=$\frac{G}{\sqrt{2}}$, o-*] (2,2);
   \draw (3,2.25) to[short, o-*] (2,2);
   \draw (0,0) node[anchor=north] {$\varphi_{lower\ left\ old}$};
   \draw (4,0) node[anchor=north] {$\varphi_{lower\ right\ old}$};
   \draw (4,4) node[anchor=south] {$\varphi_{upper\ right\ old}$};
   \draw (0,4) node[anchor=south] {$\varphi_{upper\ left\ old}$};
   \draw (2,4) node[anchor=south] {$\varphi_{up\ old}$};
   \draw (0,2) node[anchor=east] {$\varphi_{left\ old}$};
   \draw (2,0) node[anchor=north] {$\varphi_{down\ old}$};
   \draw (4,2) node[anchor=west] {$\varphi_{right\ old}$};
   \draw (3,2.25) node[anchor=west] {$\varphi_{centre\ old}$};
  \end{circuitikz}
  \caption{internal node in star topology}
  \label{fig:internalstar}
 \end{center}
\end{figure}

\pagebreak
Equations \ref{eq:potentialchange} and \ref{eq:ptentialchangevector} (pages \pageref{eq:potentialchange} and \pageref{eq:ptentialchangevector}) from previous section are still valid. However equations for $\Delta\varphi$ and $\Delta\vec{\varphi}$ differs. To find those equations for this topology, one may again assume that Kirchhoff's low has to be fulfilled. This leads to
\begin{eqnarray}
 \nonumber G\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{up\ old} \right ) + G\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{left\ old} \right ) &+&\\
 \nonumber + G\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{down\ old}\right ) + G\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{right\ old} \right ) &+&\\
 \nonumber + \frac{G}{\sqrt{2}}\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{upper\ left\ old} \right ) + \frac{G}{\sqrt{2}}\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{upper\ right\ old} \right ) &+&\\
 \nonumber + \frac{G}{\sqrt{2}}\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{lower\ left\ old} \right ) + \frac{G}{\sqrt{2}}\left (\varphi_{centre\ old} + \Delta\varphi - \varphi_{lower\ right\ old} \right ) &=& 0
\end{eqnarray}
\begin{eqnarray}
 \nonumber \left (4 + \frac{4}{\sqrt{2}} \right ) G \left (\varphi_{centre\ old} + \Delta\varphi \right ) = G\left (\varphi_{up\ old} + \varphi_{left\ old} + \varphi_{down\ old} + \varphi_{right\ old}\right ) +\\
 \nonumber + \frac{G}{\sqrt{2}}\left (\varphi_{upper\ left\ old} + \varphi_{upper\ right\ old} + \varphi_{lower\ left\ old} + \varphi_{lower\ right\ old}\right )
\end{eqnarray}
\begin{eqnarray}
 \nonumber\Delta\varphi = \frac{\varphi_{up\ old} + \varphi_{left\ old} + \varphi_{down\ old} + \varphi_{right\ old}}{4 + 2\sqrt{2}} + \\
 + \frac{\frac{1}{\sqrt{2}}\left (\varphi_{upper\ left\ old} + \varphi_{upper\ right\ old} + \varphi_{lower\ left\ old} + \varphi_{lower\ right\ old}\right )}{4 + 2\sqrt{2}} - \varphi_{centre\ old}
 \label{eq:stardelta}
\end{eqnarray}

In general assumption equation \ref{eq:crossdeltavector}, that is
\begin{eqnarray}
 \nonumber&\Delta\vec{\varphi} = \vec{g}\mat{\varphi} - \vec{\varphi}_{centre\ old}&
\end{eqnarray}
is still valid, however both vector $\vec{g}$ and matrix $\mat{\varphi}$ are defined in different way. First of all vector $\vec{g}$ is vector of not four, but eight elements

\begin{eqnarray}
 \nonumber\vec{g} = \sum_{i = 1}^{4}\frac{1}{4 + 2\sqrt{2}} \baseVec + \sum_{i = 5}^{8}\frac{\frac{1}{\sqrt{2}}}{4 + 2\sqrt{2}} \baseVec
\end{eqnarray}
and matrix $\mat{\varphi}$ is now having four columns, but eight rows that are $\vec{\varphi}_{up\ old}$, $\vec{\varphi}_{down\ old}$, $\vec{\varphi}_{left\ old}$, $\vec{\varphi}_{right\ old}$, $\vec{\varphi}_{upper\ left\ old}$, $\vec{\varphi}_{upper\ right\ old}$, $\vec{\varphi}_{lower\ left\ old}$ and $\vec{\varphi}_{lower\ right\ old}$ respectively. Vectors $\vec{\varphi}_{up\ old}$, $\vec{\varphi}_{down\ old}$, $\vec{\varphi}_{left\ old}$, $\vec{\varphi}_{right\ old}$ and $\vec{\varphi}_{centre\ old}$ are defined in the same way as previously. Other vectors are
\begin{eqnarray}
 \nonumber\vec{\varphi}_{upper\ left\ old} &=& \varphi^{R}_{upper\ left\ old} \baseVec[1] + \varphi^{G}_{upper\ left\ old} \baseVec[2] +\\
 \nonumber&&+\varphi^{B}_{upper\ left\ old} \baseVec[3] + \varphi^{A}_{upper\ left\ old} \baseVec[4]\\
 \nonumber\vec{\varphi}_{upper\ right\ old} &=& \varphi^{R}_{upper\ right\ old} \baseVec[1] + \varphi^{G}_{upper\ right\ old} \baseVec[2] +\\
 \nonumber&&+\varphi^{B}_{upper\ right\ old} \baseVec[3] + \varphi^{A}_{upper\ right\ old} \baseVec[4]\\
 \nonumber\vec{\varphi}_{lower\ left\ old} &=& \varphi^{R}_{lower\ left\ old} \baseVec[1] + \varphi^{G}_{lower\ left\ old} \baseVec[2] +\\
 \nonumber&&+\varphi^{B}_{lower\ left\ old} \baseVec[3] + \varphi^{A}_{lower\ left\ old} \baseVec[4]\\
 \nonumber\vec{\varphi}_{lower\ right\ old} &=& \varphi^{R}_{lower\ right\ old} \baseVec[1] + \varphi^{G}_{lower\ right\ old} \baseVec[2] +\\
 \nonumber&&+\varphi^{B}_{lower\ right\ old} \baseVec[3] + \varphi^{A}_{lower\ right\ old} \baseVec[4]
\end{eqnarray}

Unfortunately equation \ref{eq:crossdeltavector} being in the form presented above is not trivial in implementation. Due to that compiler may fail to optimally adapt it to hardware possibilities and all profits of vector form may be lost. Fortunately one may notice that slight modification of this equation makes it far simpler to implement. One may safely rewrite it to the form
\begin{equation}
 \Delta\vec{\varphi} = \vec{g}_1\mat{\varphi_1} + \vec{g}_2\mat{\varphi_2} - \vec{\varphi}_{centre\ old}
 \label{eq:stardeltavector}
\end{equation}
where
\begin{eqnarray}
 \nonumber\vec{g}_1 &=& \sum_{i = 1}^{4}\frac{1}{4 + 2\sqrt{2}} \baseVec\\
 \nonumber\vec{g}_2 &=& \sum_{i = 1}^{4}\frac{\frac{1}{\sqrt{2}}}{4 + 2\sqrt{2}} \baseVec = \sum_{i = 1}^{4}\frac{1}{4 + 4\sqrt{2}} \baseVec
\end{eqnarray}
and matrix $\mat{\varphi_1}$ is having four rows, being $\vec{\varphi}_{up\ old}$, $\vec{\varphi}_{down\ old}$, $\vec{\varphi}_{left\ old}$ and $\vec{\varphi}_{right\ old}$. At the same time for matrix $\mat{\varphi_2}$ rows are $\vec{\varphi}_{upper\ left\ old}$, $\vec{\varphi}_{upper\ right\ old}$, $\vec{\varphi}_{lower\ left\ old}$ and $\vec{\varphi}_{lower\ left\ old}$. Thanks to this above equation can be computed by program similar to one from listings \ref{code:simpleinternalvector} and \ref{code:simplebordervector} (pages \pageref{code:simpleinternalvector} and \pageref{code:simplebordervector}). Problem of borders conditions may be solved in the same way as presented before in case of cross topology. Because of that above equations may be considered as universal and only electric potentials of missing nodes must be set properly.

\section{Arbitrary topology}
Except presented so far topologies there may be many others. Assuming uniform material without influence of magnetic field one may consider it as any net of resistors, not only presented before. As before any resistor with index $i \in \mathbb{N} \cap \langle 1, n \rangle$ that is connecting to neighbouring node at relative distance $l_i$ has constant conductance $\frac{G}{l_i}$. Having $n$ neighbouring nodes Kirchhoff's law may be expressed as
\begin{eqnarray}
 \nonumber \sum_{i = 1}^{n} \frac{G}{l_i} \left (\varphi_{centre\ old} + \Delta\varphi - \varphi_i \right ) &=& 0\\
 \nonumber \sum_{i = 1}^{n} \frac{G}{l_i} \Delta\varphi &=& \sum_{i = 1}^{n} \frac{G\varphi_i}{l_i} - \left (\sum_{i = 1}^{n}\frac{G\varphi_{centre\ old}}{l_i}\right )\\
 \nonumber \left (\vec{w} = \sum_{i = 1}^{n} w_i \baseVec = \sum_{i = 1}^{n} \frac{1}{l_i} \baseVec \land \vec{\varphi} = \sum_{i = 1}^{n} \varphi_i \baseVec \right ) &\Rightarrow& \Delta\varphi = \frac{\dotMul{\vec{\varphi}}{\vec{w}}}{\sum_{i = 1}^{n}w_i} - \varphi_{centre\ old}
\end{eqnarray}
the above leads to
\begin{equation}
 \Delta\varphi = \arithMean[\vec{w}]{\vec{\varphi}} - \varphi_{centre\ old}
 \label{eq:arbitrarydelta}
\end{equation}
where $\varphi_i$ is electric potential at neighbouring node having index $i \in \mathbb{N} \cap \langle 1, n \rangle$. Please note that equations \ref{eq:crossdelta} and \ref{eq:stardelta} (pages \pageref{eq:crossdelta} and \pageref{eq:stardelta}) are just special cases of the above one. Additionally one may notice that equations \ref{eq:potentialchange} and \ref{eq:ptentialchangevector} (pages \pageref{eq:potentialchange} and \pageref{eq:ptentialchangevector}) that are describing change of electric potential in each iteration, remain independent on any topology.

For better and easier implementation, equation \ref{eq:arbitrarydelta} can be rewritten into vector form just like \ref{eq:crossdelta} and \ref{eq:stardelta}. General form of this equation remains the same
\begin{eqnarray}
 \nonumber\Delta\vec{\varphi} &=& \vec{g}\mat{\varphi} - \vec{\varphi}_{centre\ old}
\end{eqnarray}
where
\begin{eqnarray}
 \nonumber \vec{g} &=& \frac{1}{\sum_{i = 1}^{n} w_i} \vec{w}
\end{eqnarray}
and $\mat{\varphi}$ is matrix where each row with index $i \in \mathbb{N} \cap \langle 1, n \rangle$ is vector $\vec{\varphi}_i$ with elements being $\varphi_i$ for every node in fragment. Because of that matrix $\mat{\varphi}$ is having $n$ rows and $m$ columns, where $m$ is number of nodes in fragment and $n$ is number of neighbouring nodes per each. Of course above equation may be simplified for implementation in the same way as in case of equation \ref{eq:stardeltavector} (page \pageref{eq:stardeltavector})
\begin{equation}
 \Delta\vec{\varphi} = \left (\sum_{i = 1}^{k}\vec{g}_i\mat{\varphi_i} \right ) - \varphi_{centre\ old}
 \label{eq:arbitrarydeltavector}
\end{equation}
where $k$ is number of easily implementable groups on which $n$ (number of neighbouring nodes) is divided. In case of equation \ref{eq:stardeltavector} $k = 2$ because eight neighbouring nodes per each of four nodes in fragment could easily give two groups of matrix four by four and vector of four elements each. However this strongly depends on number $n$ and $m$ (number of nodes per fragment) and hence can not be general and must be adapted to each case.
