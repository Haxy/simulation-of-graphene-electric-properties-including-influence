\chapter{Calculation methods}
In calculations of electrical circuits there are few possible approaches. Most common is to create and solve set of equations. Solutions to those equations are states describing each element of circuit. In general case those solutions are functions of time. In case of AC analysis flow of current is time dependent, so state of each element may also change in time. However in case of DC analysis we may safely assume that state of each element is time independent.

\section{Linear equations}
Most common way is to construct and solve set of linear equations. Such sets are mostly represented as matrix. Each set of linear equations \footnote{In simplest case there should be as many unknown variables, as equations. This is not general case, however for purposes of this thesis such assumption is satisfactory.}
\begin{eqnarray*}
a_{(1,1)} x_1 + a_{(1,2)} x_2 + \cdots + a_{(1,n)} x_n& = &b_1\\
\nonumber a_{(2,1)} x_1 + a_{(2,2)} x_2 + \cdots + a_{(2,n)} x_n& = &b_2\\
\nonumber &\vdots& \\
\nonumber a_{(n,1)} x_1 + a_{(n,2)} x_2 + \cdots + a_{(n,n)} x_n& = &b_n
\end{eqnarray*}
can be simply considered as
\begin{eqnarray*}
\mat{A} \vec{x} = \vec{b}
\end{eqnarray*}
where
\begin{eqnarray*}
\nonumber \mat{A}& = &\sum^{n}_{i = 1} \sum^{n}_{j = 1} a_{(i,j)} \baseMat \\
\nonumber \vec{b}& = &\sum^{n}_{i = 1} b_{i} \baseVec \\
\nonumber \vec{x}& = &\sum^{n}_{i = 1} x_{i} \baseVec
\end{eqnarray*}
and $\vec{x}$ is unknown vector to be calculated. In order to find $\vec{x}$ one must find $(\mat{A})^{-1}$ (inversion of matrix $\mat{A}$) and multiply it by $\vec{b}$.
\begin{eqnarray*}
\vec{x} = \left (\mat{A} \right )^{-1} \vec{b}
\end{eqnarray*}
In general case this non trivial and finding $\left (\mat{A} \right )^{-1}$ may contain large numerical error. Therefore there are several methods of solving such equations were invented.
\subsection{LU factorisation}
Most common software for electronics circuit calculations(Spice) uses LU factorisation. First step of this algorithm is to find matrices\footnote{Not all matrices may be decomposed in such way, however ones considered by Spice software may be decomposed, because they are rare matrices (most of their elements are zeros).} $\mat{L}$ and $\mat{U}$ such as
\begin{eqnarray*}
\nonumber \mat{A} = \mat{L}\mat{U}&& \\
\nonumber \left ( \mat{L} = \sum^{n}_{i = 1} \sum^{n}_{j = 1} l_{(i,j)} \baseMat \right ) \land \left (j > i \right ) &\Rightarrow& l_{(i,j)} = 0\\
\nonumber \left (\mat{U} = \sum^{n}_{i = 1} \sum^{n}_{j = 1} u_{(i,j)} \baseMat \right ) \land \left (j < i \right ) &\Rightarrow& u_{(i,j)} = 0
\end{eqnarray*}

\pagebreak
\noindent then second step is to solve equations
\begin{eqnarray*}
\nonumber \mat{L}\vec{y}& = &\vec{b}\\
\nonumber \mat{U}\vec{x}& = &\vec{y}
\end{eqnarray*}
where
\begin{eqnarray*}
\vec{y} = \sum^{n}_{i = 1} y_{i} \baseVec
\end{eqnarray*}
is unknown vector to be calculated for second equation. Although one have two sets of equations to be solved they are far simpler because both $\mat{L}$ and $\mat{U}$ are triangular matrices\footnote{Triangular matrices have all elements of either upper or lower part being zero.}. Calculations may be performed as
\begin{eqnarray*}
\nonumber y_{1}& = &\frac{b_{1}}{l_{(1,1)}}\\
\nonumber y_{2}& = &\frac{b_{2} - y_{1}l_{(2,1)}}{l_{(2,2)}}\\
\nonumber &\vdots&\\
\nonumber y_{n}& = &\frac{b_{n} - \left (\sum^{n - 1}_{i = 1}y_{i}l_{(n,i)} \right )}{l_{(n,n)}}\\
\end{eqnarray*}
and later
\begin{eqnarray*}
\nonumber x_{n}& = &\frac{y_{n}}{u_{(n,n)}}\\
\nonumber x_{n - 1}& = &\frac{y_{n -1} - x_{n}u_{(n - 1,n)}}{u_{(n - 1,n - 1)}}\\
\nonumber &\vdots&\\
\nonumber x_{1}& = &\frac{y_{1} - \left (\sum^{n}_{i = 2}x_{i}u_{(1,i)} \right )}{u_{(1,1)}}\\
\end{eqnarray*}

\pagebreak
There are known many algorithms to find matrices $\mat{L}$ and $\mat{U}$ however those algorithms will not be described in this thesis. Moreover LU factorisation is not the only way to solve set of linear equations.
\subsection{Conjugate gradient method}
Another commonly used method for solving sets of linear equations is so called `conjugate gradient method'. This method is iterative algorithm. It means that it creates a set of vectors converging to solution of equations. The simplest version of this algorithm may be expressed as
\begin{eqnarray*}
\nonumber \vec{v}_0& = &\vec{b} - \left (\mat{A} \vec{x}_0 \right )\\
\nonumber \vec{r}_0& = &\vec{v}_0\\
\nonumber t_k& = &\frac{\dotMul{\vec{r}_{(k - 1)}}{\vec{r}_{(k - 1)}}}{\vec{v}_{(k - 1)} \mat{A} \vec{v}_{(k - 1)}}\\
\nonumber \vec{r}_k& = &\vec{r}_{(k - 1)} - t_{(k - 1)} \mat{A} \vec{v}_{(k - 1)}\\
\nonumber \vec{v}_k& = &\vec{r}_k + \frac{\dotMul{\vec{r}_k}{\vec{r}_k}}{\dotMul{\vec{r}_{(k - 1)}}{\vec{r}_{(k - 1)}}} \vec{v}_{(k - 1)}\\
\nonumber \vec{x}_k& = &\vec{x}_{(k - 1)} + t_k \vec{v}_{(k - 1)}
\end{eqnarray*}
where $\vec{x_0}$ may be set randomly. If \dotMul{$\vec{v}_k}{\vec{v}_k} = 0$ then $\vec{x}_k$ is exact solution of equation. However iterations may be stopped if $\dotMul{\vec{r}_k}{\vec{r}_k}$ or $\dotMul{\vec{v}_k}{\vec{v}_k}$ is small enough.

Since this method is commonly used, it has many modifications. Most of the approaches change the computation complexity of each iteration and number of iterations. For some problems and architectures it is more convenient to have smaller number of more complex iterations while for some others opposite approach gives better results.

\section{Non-linear equations}
Solving sets of non-linear equations can be reduced to iterative operations on matrices. For instance Newton's algorithm can be extended to matrix algebra and hence may be used to calculate sets of non-linear equations. One may assume set of non-linear equations to be of form
\begin{eqnarray*}
f_1(\vec{x}) &=& 0\\
f_2(\vec{x}) &=& 0\\
&\vdots&\\
f_n(\vec{x}) &=& 0
\end{eqnarray*}
then Newton's method may be expressed as
\begin{eqnarray*}
\vec{x}_{k + 1} = \vec{x}_k - \left (\mat{J}\left (\vec{x}_k \right ) \right )^{-1} \vec{F}(\vec{x}_k)
\end{eqnarray*}
where
\begin{eqnarray*}
\vec{F}\left (\vec{x} \right ) &=& \sum^{n}_{i = 1}f_i\left (\vec{x} \right ) \baseVec\\
\vec{x} = \sum^{m}_{i = 1} x_i \baseVec \Rightarrow \mat{J}(\vec{x}) &=& \sum^{n}_{i = 1} \sum^{m}_{j = 1} \frac{\partial f_i\left (\vec{x} \right )}{\partial x_j} \baseMat
\end{eqnarray*}
In general case $\mat{J}\left (\vec{x} \right )$ may be arbitrary matrix, however for Newton's method this must be square matrix. So number of equations and unknown variables must be same. Initial value of $\vec{x}$ (that is $\vec{x}_0$) may be chosen arbitrary.

\section{Relaxation}
All methods mentioned in previous sections solve sets of equations. To find out state of $n$ elements one must create matrix having $n^2$ elements (square matrix $n$ by $n$). Solving such set is computationally expensive. Even when processing massively parallel way on GPU, gain is not large. According to \cite{Sparse12} speed up in computation is only 2.7 times on average. Assuming only neighbouring elements depends directly on each other, one may construct rare matrix, so the matrix that have most of its elements being equal to zero. However this only simplify creation of matrix, but do not give much while solving it. There are algorithms that uses benefits of rare matrices, however they may hardly be rewritten in parallel form.

On the other hand changes in physical system may be reflected by its successive relaxation. In this iterative approach each element of such system may be calculated independently on others. Hence this approach (called later in this document `relaxation method') allows for highly parallel computation. In comparison to previously mentioned methods it requires larger number of iterations, but each one is simpler. Additionally it requires computations on sets of $n$ elements unlike previously mentioned algorithms requiring $n^2$ elements. Thanks to this requirement for memory may be strongly reduced. Relaxation method (although not popular in literature) allows to solve larger and more complex systems.

Additionally relaxation method has slower convergence, hence it requires larger number of iterations. Hopefully each iteration is far simpler in computation. Thanks to this total time required for computations is shorter. Those benefits makes this method a perfect candidate for solving problems described in this thesis.
