\chapter{GLSL} Code listings presented in this thesis will use OpenGL Shading
Language (called later by its official abbreviation --- GLSL). This language is
based on syntax of C and C++ programming languages. GLSL is actually set of
closely related languages, however listings in this thesis may be considered as
using fragment or compute shaders (both making use of nearly same resources) and
hence whole set of languages will be considered as single one.

Official full documentation is presented in \cite{kessenich2016GLSL}.
Recommended sources are also \cite{opengl-orange} and \cite{opengl-web}. This
chapter is to bring just minimal information required to understand code
presented in listings.

\section{Basic types} GLSL is designed to support vector and matrix algebra but
have neither text nor memory operations therefore set of possible types is quite
different than in C or C++.

\subsection{Scalar types} There are only 5 basic scalar types in GLSL. Moreover,
some of them are not available in earlier versions. Scalar types and
descriptions are presented in table \ref{tab:GLSLscalartypes}
(page~\pageref{tab:GLSLscalartypes}).
\begin{table}[!ht]
 \centering
 \begin{tabular}{rp{10cm}}
  type name & meaning \\ \hline
  `\variable{bool}' & Single boolean value (only two possible values `\variable{true}'
        or~`\variable{false}'). Standard of any GLSL version does not specify
        its size but by most of implementations it is realised as 32--bit long
        integer in which value $0$ corresponds to `\variable{false}' and any
        value different than $0$ to `\variable{true}'.\\
  `\variable{int}' & Single 32--bit, 2's complement integer (Since version 1.30, in earlier
        versions size and sign encoding were unspecified).\\
  `\variable{uint}' & Single 32--bit long unsigned integer (no sign encoded, only
        non--negative integers --- using natural binary code). This type is part
        of standard since version 1.30.\\
  `\variable{float}' & Floating--point number. Since version 1.30 specified as 
        IEEE--754\tablefootnote{More on IEEE--754 standard
	 may be found at \cite{IEEE2008754} \label{somethingabtIEEE} }
        single--precision.\\
  `\variable{double}' & IEEE--754\tablefootnote{See footnote \footref{somethingabtIEEE}} double--precision number, part of
        standard since version 4.00.
 \end{tabular}
 \caption{Basic scalar types in GLSL.}
 \label{tab:GLSLscalartypes}
\end{table}
\lstinputlisting[caption={Examples of scalar variables.}, label=code:scalarinit, escapechar=, style=customGLSL]{scalarinit.gls} 




The above listing presents some of possible scalar declarations and
initialisations. It is important to notice that floating--point literal
constants (unlike in C and C++) by default are single--precision numbers.

\subsection{Vector types} In GLSL there are 15 possible vector types. Again as
in case of scalar variables not all vector types are available in earlier
versions of language.  \begin{table}[!ht] \centering
    \begin{tabular}{rp{7cm}|p{3cm}} type name & meaning & comments \\ \hline
        \hline `\variable{bvec2}' & Vector of two boolean numbers.&\\
        `\variable{bvec3}' & Vector of three boolean numbers.&\\
        `\variable{bvec4}' & Vector of four boolean numbers.&\\ \hline
        `\variable{ivec2}' & Vector of two integers.&\\ `\variable{ivec3}' &
        Vector of three integers.&\\ `\variable{ivec4}' & Vector of four
        integers.&\\ \hline `\variable{uvec2}' & Vector of two non--negative
        integers.&\multirow{3}{3cm}{Part of standard since version 1.30}\\
        `\variable{uvec3}' & Vector of three non--negative integers.&\\
        `\variable{uvec4}' & Vector of four non--negative integers.&\\ \hline
        `\variable{vec2}' & Vector of two floating--point single--precision
        numbers.&\\ `\variable{vec3}' & Vector of three floating--point
        single--precision numbers.&\\ `\variable{vec4}' & Vector of four
        floating--point single--precision numbers.&\\ \hline `\variable{dvec2}'
        & Vector of two floating--point double--precision
        numbers.&\multirow{3}{3cm}{Part of standard since version 4.00}\\
    `\variable{dvec3}' & Vector of three floating--point double--precision
    numbers.&\\ `\variable{dvec4}' & Vector of four floating--point
    double--precision numbers.&\\ \end{tabular} \caption{Basic vector types in
GLSL.} \label{tab:GLSLvectortypes} \end{table}
\lstinputlisting[caption={Examples of single--precision floating--point vector variables.}, label=code:vecinit, escapechar=, style=customGLSL]{vecinit.gls} 

Above code listing present some of ways to declare and initialise
single--precision floating--point vectors. Same rules hold for other vector
types. It is worth mentioning that there are other possible ways to initialise
vector variables in GLSL but they are not used in this thesis therefore (for
simplicity) they are not presented.

\subsection{Matrix types} GLSL defines 18 possible matrix types. Unlike vectors
matrices may store only floating--point numbers (single-- or double--precision
however double--precision matrices are supported since version 4.00).
\subsubsection{Matrices of single--precision floating--point numbers} All types
representing matrices of single--precision floating--point are presented in
table below.  \begin{table}[!ht] \centering \begin{tabular}{rp{7cm}} type name &
    meaning \\ \hline `\variable{mat2}' & matrix with 2 rows and 2 columns\\
    `\variable{mat2x2}' & same as `\variable{mat2}'\\ `\variable{mat2x3}' &
    matrix with 3 rows and 2 columns\\ `\variable{mat3x2}' & matrix with 2 rows
    and 3 columns\\ `\variable{mat2x4}' & matrix with 4 rows and 2 columns\\
    `\variable{mat4x2}' & matrix with 2 rows and 4 columns\\ `\variable{mat3}' &
    matrix with 3 rows and 3 columns\\ `\variable{mat3x3}' & same as
    `\variable{mat3}'\\ `\variable{mat3x4}' & matrix with 4 rows and 3 columns\\
`\variable{mat4x3}' & matrix with 3 rows and 4 columns\\ `\variable{mat4}' &
matrix with 4 rows and 4 columns\\ `\variable{mat4x4}' & same as
`\variable{mat4}' \end{tabular} \caption{Basic single--precision matrix types in
GLSL.} \label{tab:GLSLmatrixtypes} \end{table}

Types `\variable{mat2}' and `\variable{mat2x2}' may be used interchangeably.
Same holds for pairs `\variable{mat3}' --- `\variable{mat3x3}' and
`\variable{mat4}' --- `\variable{mat4x4}'.  
\lstinputlisting[caption={Examples of single--precision floating--point matrix variables.}, label=code:matinit, escapechar=, style=customGLSL]{matinit.gls} 

The above code listing present few ways to define and initialise matrix
variable. There are more, however they are not presented sine they are not used
in this thesis.

\subsubsection{Matrices of double--precision floating--point numbers} All types
representing matrices of double--precision floating--point are presented in
table below. Those matrix types are port of language standard since version
4.00.  \begin{table}[!ht] \centering \begin{tabular}{rp{7cm}} type name &
    meaning \\ \hline `\variable{dmat2}' & matrix with 2 rows and 2 columns\\
    `\variable{dmat2x2}' & same as `\variable{dmat2}'\\ `\variable{dmat2x3}' &
    matrix with 3 rows and 2 columns\\ `\variable{dmat3x2}' & matrix with 2 rows
    and 3 columns\\ `\variable{dmat2x4}' & matrix with 4 rows and 2 columns\\
    `\variable{dmat4x2}' & matrix with 2 rows and 4 columns\\ `\variable{dmat3}'
    & matrix with 3 rows and 3 columns\\ `\variable{dmat3x3}' & same as
    `\variable{dmat3}'\\ `\variable{dmat3x4}' & matrix with 4 rows and 3
    columns\\ `\variable{dmat4x3}' & matr/ix with 3 rows and 4 columns\\
    `\variable{dmat4}' & matrix with 4 rows and 4 columns\\ `\variable{dmat4x4}'
& same as `\variable{dmat4}' \end{tabular} \caption{Basic double--precision
matrix types in GLSL.} \label{tab:GLSLdmatrixtypes} \end{table}

Double--precision matrix variables are declared and initialised in similar way
as single--precision ones (presented by code listing \ref{code:matinit} --- page
\pageref{code:matinit})

\subsection{Sampler types} Sampler variables are handles to textures (one--,
two-- or three--dimensional), cube maps and other similar elements. Sampler
variables can not be initialised from within or reassigned by shader. Actually
such variables may be considered as read--only (initialised and maintained by
program invoking shader). This thesis make use only of two sampler types.
\begin{table}[!ht]
 \centering
 \begin{tabular}{rp{10cm}}
  type name & meaning \\ \hline
  `\variable{sampler2D}' & handle to two--dimensional texture of single--precision floating--point vectors\\
  `\variable{sampler3D}' & handle to two--dimensional texture of single--precision floating--point vectors
 \end{tabular}
 \caption{Used sampler types.}
 \label{tab:GLSLsamplertypes}
\end{table}

Textures are recommended for storing large amount of data. Maximal size of
texture is actually not specified, however most of modern platforms may handle
textures up to $549755813888$ ($2^{39}$) fragments of type `\variable{vec4}'
each (in discussed case). Every shader may use more than one texture at once.
Maximal number of textures bind simultaneously to single shader depends on
platform but standard of OpenGL suggests not less than eight.

\section{Storage qualifiers} Each variable declaration may use only one storage
qualifiers. The GLSL standard defines 9 possible storage qualifiers, however
only three of them are used in this thesis.

\subsection{`\variable{const}' storage qualifier} Indicates that variable can
not change its value after initialisation (read--only variable). According to
standard of language variable with `\variable{const}' storage qualifier may be
initialised in any way. This gives possibility to use it with variable which
initialisation will be evaluated during run--time. Unfortunately some compilers
claim that initialisation of variable with `\variable{const}' storage qualifier
must be evaluated during compile--time. Due to this, those variables may be
initialised with constant expression only. For purpose of compatibility such
assumption will also be used in this thesis.

Variables with `\variable{const}' storage qualifier co not be accessed from
outside of shader.

\subsection{`\variable{uniform}' storage qualifier} This qualifier may refer
only to global variables. Value of variable with this qualifier is same for all
invocations of shader. Such variable is read--only. Its value may be initialised
within shader by means of constant expression. In such case it will be assigned
during link--time. However variables with `\variable{uniform}' storage qualifier
are in most cases to be set by external program invoking shader. Only such usage
will be assumed in this thesis.

The amount of storage for `\variable{uniform}' variables depends on platform,
however they are not supposed to transfer huge amount of data.

Additionally it is worth remembering that declared but unused
`\variable{uniform}' variables are removed from shader during compilation and
hence can not be accessed by external program invoking shader.

\subsection{Default storage qualifier} Default storage qualifier is not
indicated with any keyword. Variable with default storage qualifier has
read/write access. Additionally such variable can not be accessed from outside
of shader.

\section{Build--in functions} GLSL offers large number of different standard
functions. Therefore in this chapter there will be discussed only those used in
this thesis.

\subsection{`\variable{dot}'} Function of two variables of same type. Arguments
may only be of type `\variable{vec2}', `\variable{vec3}', `\variable{vec4}',
`\variable{dvec2}', `\variable{dvec3}' or `\variable{dvec4}'. Result of this
function is dot product of both arguments. Function returns single value of type
`\variable{float}' if arguments were single--precision vectors (type
`\variable{vec2}', `\variable{vec3}' or `\variable{vec4}') or of type
`\variable{double}' if arguments were double--precision vectors (type
`\variable{dvec2}', `\variable{dvec3}' or `\variable{dvec4}').

Additionally it should be emphasized that since many matrix and vector
operations performed by GPUs make use of dot product, those processors usually
have hardware unit to perform such multiplication. Therefore it is recommended
to use this standard function whenever problem may be considered as dot product
of vectors.

\subsection{`\variable{abs}'} Function of single variable of type
`\variable{float}', `\variable{double}' or `\variable{int}'. It realises
equation below \begin{eqnarray} \nonumber \mathrm{abs} \left ( x \right ) &=&
    \left \{ \begin{array}{ccc} x & \Rightarrow & x \geq 0 \\ -x & \Rightarrow &
    x < 0 \end{array} \right .  \end{eqnarray}

Although this seems as conditional function it should be noticed that IEEE--754
    standard uses single bit to encode sign. Therefore this operation my be
    performed by just setting single bit to $0$. This operation will give right
    results even for special values turning $\pm 0$ to $0$, $\pm\infty$ to
    $\infty$ and any NaN to the same NaN since in case of NaNs sign bit is
    irrelevant\footnote{More on IEEE--754 standard may be found at
    \cite{IEEE2008754}.}. Unfortunately this function is not that trivial for
    arguments of type `\variable{int}'.

\subsection{`\variable{texture}'} This function takes two arguments, sampler
    handling to particular texture and vector of coordinates. Result of this
    function is vector stored (at passed coordinates) in texture bound to
    sampler. It is important to emphasize that texture coordinates are in range
    $\left\langle 0,\, 1\right\rangle$. Rules of interpolation and extrapolation
    are to be set by external program invoking shader. In this thesis results
    from within range are always interpolated to nearest fragment. Additionally,
    since code presented in this thesis do not exit range, extrapolation method
    may be neglected (unless stated otherwise).

Function `\variable{texture}' is part of standard since version 1.30. Earlier
    versions were instead using functions `\variable{texture2D}',
    `\variable{texture3D}', etc.

\subsection{`\variable{matrixCompMult}'} This is function of two variables of
    same matrix type each. It performs element--wise multiplication of two
    passed matrices. This function is available in all versions of~GLSL.

\subsection{`\variable{fma}'} \label{ssec:glslfma}
This is function of three variables of same type. Its name stands for `fused multiply--add' In general case this function may considered as
\begin{eqnarray}
 \nonumber \mathrm{fma}\left ( a,\, x,\, b \right ) &=& ax + b
\end{eqnarray}

This function may be realisaed by means of `\variable{dot}' function as presented by code listing below
\lstinputlisting[caption={Possible implementation of `\variable{fma}' function.}, label=code:swizzling, escapechar=, style=customGLSL]{fmaasdot.gls} 
However also `\variable{dot}' function may be realised by means `\variable{fma}' function as presented by code listing below
\lstinputlisting[caption={Possible implementation of `\variable{dot}' function.}, label=code:swizzling, escapechar=, style=customGLSL]{dotasfma.gls} 

In some architectures like `Maxwell', `Kepler'` and `Pascal' FPUs will consider `\variable{fma}' function as single operation. Therefore its usage may be considered as faster and giving more accurate result (rounding performed only once).

\section{Swizzling and indexing}
Swizzling is form of reordering fields of particular type. In some cases may lead to creation of another types. Possible swizzling masks are presented in table below
\begin{table}[!ht]
 \centering
 \begin{tabular}{p{7cm}|p{2cm}p{2cm}p{2cm}}
  type name & points and~normals&colours& textute coordinates \\ \hline
  `\variable{bvec4}', `\variable{ivec4}', `\variable{uvec4}', `\variable{vec4}', `\variable{dvec4}' & xyzw & rgba & stpq\\
  `\variable{bvec3}', `\variable{ivec3}', `\variable{uvec3}', `\variable{vec3}', `\variable{dvec3}' & xyz & rgb & stp\\
  `\variable{bvec2}', `\variable{ivec2}', `\variable{uvec2}', `\variable{vec2}', `\variable{dvec2}' & xy & rg & st\\
  `\variable{bool}', `\variable{int}', `\variable{uint}', `\variable{float}', `\variable{double}' & x\tablefootnote{Scalars are considered as vectors of single element. \label{note:scalarsasvectors}} & r\tablefootnote{See footnote \footref{note:scalarsasvectors}.} & s\tablefootnote{See footnote \footref{note:scalarsasvectors}.}
 \end{tabular}
 \caption{Swizzling masks in GLSL.}
 \label{tab:GLSLswizzlingmasks}
\end{table}

Swizzling masks can be picked arbitrary but can not be mixed. Few examples of possible usage are presented in code listing below.
\lstinputlisting[caption={Examples of swizzling.}, label=code:swizzling, escapechar=, style=customGLSL]{swizzling.gls} 

If swizzling r--value swizzled fields may repeat, however in case of l--value such repetition would lead to ambiguity. Additionally, usage of swizzling that would lead to not existing type (even for anonymous variable) is illegal.

In case of vectors index $0$ corresponds to fields `\variable{x}', `\variable{r}' and `\variable{s}', index $1$ to `\variable{y}', `\variable{g}' and `\variable{t}', index $2$ to `\variable{z}', `\variable{b}' and `\variable{p}' while index $3$ to `\variable{w}', `\variable{a}' and `\variable{q}'.

In case of matrices index $0$ corresponds to vector being leftmost column, index $1$ to second to leftmost column, etc. Due to this matrix may be considered as similar to C two--dimensional table.

\section{Single-- and double--precision numbers}
Although support for double--precision floating--point number was introduced to GLSL with version 4.00 (latest version is 4.50) there still are some issues with them. For example there are no textures of `\variable{dvec4}' fragments. Code presented in listings of this thesis uses single--precision. However if single--precision is not sufficient there are possible several solutions to this problem. GLSL offers function for packing and unpacking. Function `\variable{unpackDouble2x32}' casts a variable from type `\variable{double}' to type `\variable{uvec2}' preserving its binary representation. Function `\variable{packDouble2x32}' performs a reverse cast. Handling textures storing unsigned integers can be done by means of types `\variable{usampler2D}', `\variable{usampler3D}', etc.

Additionally it is worth mentioning that usage of double--precision numbers with some of GPUs (especially elder ones) may significantly reduce performance.
